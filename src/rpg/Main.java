package rpg;

import rpg.gfx.Screen;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Main extends Canvas implements Runnable {

    public static Random r = new Random ();
    private static JFrame frame;
    private static final int WIDTH = 480;
    private static final int HEIGHT = 360;
    private static final int SCALE = 2;
    private static final String TITLE = "RPG";

    private final double GAME_HERTZ = 60.0;
    private final double TIME_BETWEEN_TICKS = 1000000000 / GAME_HERTZ;
    private final double TIME_BETWEEN_RENDERS = 1000000000 / (GAME_HERTZ * 10);

    private boolean running = false;
    private boolean paused = false;

    private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
    private Input input;
    private Screen screen;
    private Game game;

    public static void main(String[] args) {
        Main main = new Main();
        main.setMinimumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
        main.setMaximumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
        main.setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));

        frame = new JFrame(TITLE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.add(main);
        frame.pack();
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setIgnoreRepaint(true);
        main.start();
    }

    private void start() {
        requestFocus();
        running = true;
        new Thread(this).start();
    }

    public void stop() {
        running = false;
    }

    private void init() {
        input = new Input(this);
        screen = new Screen(image.getGraphics());
        game = new Game(this, input, screen);
    }

    @Override
    public void run() {
        createBufferStrategy(2);
        BufferStrategy bs = getBufferStrategy();

        init();

        double lastUpdateTime = System.nanoTime();
        double lastRenderTime = System.nanoTime();

        while (running) {
            double now = System.nanoTime();

            if (!paused) {
                while (now - lastUpdateTime > TIME_BETWEEN_TICKS) {
                    tick();
                    while (now - lastRenderTime > TIME_BETWEEN_RENDERS) {
                        render(bs);
                        lastRenderTime += TIME_BETWEEN_RENDERS;
                    }
                    lastUpdateTime += TIME_BETWEEN_TICKS;
                }

                Thread.yield();
                now = System.nanoTime();
            }
        }

        System.exit(0);
    }

    private void tick() {
        input.tick();
        if (input.toggleKeys[input.controls.get("escape")]) stop();

        game.tick();
    }

    private void render(BufferStrategy bs) {
        do {
            do {
                Graphics g = image.getGraphics();
                g.clearRect(0, 0, WIDTH, HEIGHT);

                game.draw();

                g = bs.getDrawGraphics();
                g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
                g.dispose();
            } while (bs.contentsRestored());

            bs.show();
        } while (bs.contentsLost());
    }
}
