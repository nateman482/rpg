package rpg.entities;

import rpg.Game;
import rpg.Input;

public class Player extends Entity {

    private static final int[] id = {0, 0, 1, 2};

    public Player(int x, int y) {
        super(id, x, y);
    }

    @Override
    public void tick(Game game) {
        int[] movement = getMovement(game.input);
        x += movement[0];
        y += movement[1];
    }

    private int[] getMovement(Input input) {
        int[] movement = new int[2];

        if (input.isPressed("right")) movement[0] = 2;
        if (input.isPressed("left")) movement[0] = -2;
        if (input.isPressed("down")) movement[1] = 2;
        if (input.isPressed("up")) movement[1] = -2;

        if (movement[0] != 0 && movement[1] != 0) {
            movement[0] /= 2;
            movement[1] /= 2;
        }

        return movement;
    }
}
