package rpg.entities;

import rpg.Game;

public class Entity {

    public final int[] id;
    public int x, y;

    public Entity(int[] id, int x, int y) {
        this.id = id;
        this.x = x;
        this.y = y;
    }

    public void tick(Game game) {

    }

    public void render(Game game) {
        game.screen.drawEntity(this);
    }
}
