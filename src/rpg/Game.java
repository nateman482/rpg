package rpg;

import rpg.entities.Entity;
import rpg.entities.Player;
import rpg.gfx.Screen;

import java.util.ArrayList;
import java.util.List;

public class Game {

    public final Main main;
    public final Input input;
    public final Screen screen;

    public Player player;

    public List<Entity> entities = new ArrayList<Entity>();

    public Game(Main main, Input input, Screen screen) {
        this.main = main;
        this.input = input;
        this.screen = screen;

        player = new Player(10, 10);
        entities.add(player);
    }

    public void tick() {
        if (input.isPressed("escape")) main.stop();

        for (Entity entity : entities) {
            entity.tick(this);
        }
    }

    public void draw() {
        for (Entity entity : entities) {
            entity.render(this);
        }
    }
}
