package rpg;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;

public class Input implements KeyListener {

    public boolean[] downKeys = new boolean[65536];
    public boolean[] toggleKeys = new boolean[65536];
    public boolean[] busyKeys = new boolean[65536];

    public HashMap<String, Integer> controls = new HashMap<String, Integer>();

    public int lastPressedKey = 0;

    public Input(Main main) {
        main.addKeyListener(this);

        controls.put("escape", KeyEvent.VK_ESCAPE);
        controls.put("left", KeyEvent.VK_A);
        controls.put("right", KeyEvent.VK_D);
        controls.put("up", KeyEvent.VK_W);
        controls.put("down", KeyEvent.VK_S);
    }

    public void tick() {
        for (int i = 0; i < 65536; i++) {
            if (toggleKeys[i]) {
                toggleKeys[i] = false;
                busyKeys[i] = true;
            } else if (downKeys[i] && !busyKeys[i]) {
                toggleKeys[i] = true;
            }

            if (busyKeys[i]) {
                if (!downKeys[i]) {
                    busyKeys[i] = false;
                }
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        lastPressedKey = e.getKeyCode();
        downKeys[e.getKeyCode()] = true;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        downKeys[e.getKeyCode()] = false;
    }

    public boolean isPressed(String key) {
        return downKeys[controls.get(key)];
    }

    @Override
    public void keyTyped(KeyEvent e) { }
}
