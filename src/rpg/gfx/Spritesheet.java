package rpg.gfx;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

public class Spritesheet {

    public BufferedImage[][] getSpritesheet(String path) {
        BufferedImage sheet = null;
        BufferedImage[][] sprites;

        URL url = getClass().getResource(path);

        try {
            sheet = ImageIO.read(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        int rows = sheet.getHeight() / 32;
        int cols = sheet.getWidth() / 32;
        sprites = new BufferedImage[rows][cols];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                sprites[i][j] = sheet.getSubimage(i * 32, j * 32, 32, 32);
            }
        }

        return sprites;
    }
}
