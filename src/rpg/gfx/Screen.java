package rpg.gfx;

import rpg.entities.Entity;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Screen {

    private Graphics g;
    private BufferedImage[][] sprites;

    public Screen (Graphics g) {
        this.g = g;

        g.setFont(new Font(null, 0, 15));
        sprites = new Spritesheet().getSpritesheet("./res/sprites.png");
    }

    public void drawString(String string, int x, int y, int size) {
        g.setFont(new Font (null, 0, size));
        g.drawString(string, x, y);
    }

    public void drawEntity(Entity e) {
        for (int xx = 0; xx < e.id[2]; xx++) {
            for (int yy = 0; yy < e.id[3]; yy++) {
                g.drawImage(sprites[xx][yy], e.x + xx * 32, e.y + yy * 32, null);
            }
        }
    }
}
